﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BackEnd_Blog.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace BackEnd_Blog.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class AdminController : ControllerBase
    {
        private readonly IBlogRepository _repo;
        private readonly BlogDataContext _context;
        public AdminController(BlogDataContext context, IBlogRepository repo)
        {
            _repo = repo;
            _context = context;
        }

        //Create blog
        [HttpPost("Create")]
        [Authorize]
        public bool Create([FromForm]CreateBlogViewModel createBlogViewModel)
        {
            var authorId = User.Claims.FirstOrDefault().Value;
            createBlogViewModel.AuthorId = authorId;
            return _repo.createBlog(createBlogViewModel, createBlogViewModel.File);
        }

        //Delete blog
        [HttpDelete]
        [Authorize(Roles = "1")]
        [Route("delete/{id}")]
        public bool delete(int id)
        {
            return _repo.delete(id);

        }

        [HttpPut]
        [Authorize(Roles = "1")]
        [Route("edit/{id}")]
        public Blog edit([FromForm]BlogViewModel blog)
        {
            var b = _context.Blogs.Where(p => p.blogID == blog.blogID).FirstOrDefault();
            var authorId = User.Claims.FirstOrDefault().Value;
            blog.authorID = authorId;
            return _repo.edit(blog);
        }
    }
}