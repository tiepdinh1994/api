﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using BackEnd_Blog.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;

namespace BackEnd_Blog.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class CommentController : ControllerBase
    {
        private readonly BlogDataContext _context;
        private readonly ICommentRepository _com;

        public CommentController(BlogDataContext context, ICommentRepository com)
        {
            _context = context;
            _com = com;
        }

        //Add comment
        [HttpPost]
        [Route("AddCom")]
        [Authorize]
        public bool AddCom(CommentViewModel CreateCommentModel)
        {
            var authorID = User.Claims.FirstOrDefault().Value;
            CreateCommentModel.userID = authorID;

            return _com.AddCom(CreateCommentModel);

        }

        //get comment by id
        [HttpGet]
        [Route("get/{id}")]
        public IActionResult get(int id)
        {
            var s = _context.Comments.Where(p => p.blogID == id).OrderByDescending(t => t.creatOn).ToList();
            return Ok(s);
        }
    }
}
