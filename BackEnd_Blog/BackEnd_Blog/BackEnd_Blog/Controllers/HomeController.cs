﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BackEnd_Blog.Models;
using BackEnd_Blog.Services.IRepository;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace BackEnd_Blog.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class HomeController : ControllerBase
    {

        private readonly BlogDataContext _context;
        private readonly IBlogRepository _repo;

        public HomeController(BlogDataContext context, IBlogRepository repo)
        {
            _context = context;
            _repo = repo;
        }

        //get list blog
        [HttpGet]
        [AllowAnonymous]
        [Route("getblogs")]
        public List<BlogViewModel> getblogs(int page = 1)
        {
            if (page <= 0)
            {
                page = 1;
            }
            return _repo.GetBlogModel(page, 2);
        }


        //get page number
        [HttpGet]
        [AllowAnonymous]
        [Route("getPage")]
        public PageViewModel getPage(int page = 1)
        {
            return _repo.GetPageModel(page, 5);
        }


        //detail blog by id
        [HttpGet]
        [Route("detail/{id}")]
        [AllowAnonymous]
        public async Task<IActionResult> detail(int id)
        {
           var b = await _repo.detail(id);
            return Ok(b);

        }

        //Delete blog
        [Authorize(Roles ="1")]
        [HttpDelete("delete/{id}")]
        public bool delete(int id)
        {
            var b = _repo.delete(id);
            return b;
        }


        //<!! Get all blogs
        //[HttpGet]
        //[AllowAnonymous]
        //[Route("getblog")]
        //public IActionResult getblog()
        //{
        //    var blog = _repo.getblog();
        //    return Ok(blog);
        //}


    }

}
