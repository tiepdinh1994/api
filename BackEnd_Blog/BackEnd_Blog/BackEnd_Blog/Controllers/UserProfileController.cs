﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BackEnd_Blog.Entities;
using BackEnd_Blog.Models;
using BackEnd_Blog.Services.IRepository;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;

namespace BackEnd_Blog.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class UserProfileController : ControllerBase
    {
        private readonly BlogDataContext _context;
        private readonly IAuthenticateService _authenticateService;
        public UserProfileController(BlogDataContext context, IAuthenticateService authenticateService)
        {
            _context = context;
            _authenticateService = authenticateService;
        }

        //Login
        [AllowAnonymous]
        [HttpPost("login")]
        public IActionResult login([FromBody]AuthenticateModel model)
        {
            var pass = Helper.Encryptor.MD5Hash(model.Password);
            var token = _authenticateService.Authenticate(model.userID, model.Password = pass);

            if (token == null)
            {
                return BadRequest();
            }    
  
            return Ok(new { token = token, userID = model.userID });
        }


        //register account
        [AllowAnonymous]
        [HttpPost("register")]
        public bool register([FromBody] UserViewModel users)
        {
            return _authenticateService.register(users);
        }



        //info user
        [HttpGet("infoUser")]
        [Authorize]
        public IActionResult infoUser()
        {
            var user = _context.Users.Find(User.Claims.First().Value);
            if (user != null)
            {
                var roles = _context.RoleUser.Where(ur => ur.userID == user.roleID.ToString()).ToList();

                return Ok(new
                {
                    User = new UserViewModel
                    {
                        userID = user.userID,
                        roleID = user.roleID,
                    }
                });
            }
            else
            {
                return BadRequest();
            }

        }
    }
}