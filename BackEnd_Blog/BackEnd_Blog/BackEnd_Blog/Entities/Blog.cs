﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace BackEnd_Blog.Models
{
    public class Blog
    {
        [Key]
        public int blogID { get; set; }
        [MaxLength(150), Required]
        public string blogTitle { get; set; }
        [MaxLength(3000), Required]
        public string blogContent { get; set; }
        [Required]
        public string authorID { get; set; }
        public string blogImages { get; set; }
        public DateTime? createOn { get; set; }
        public ICollection<Comment> Comments { get; set; }
    }
}
