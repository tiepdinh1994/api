﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace BackEnd_Blog.Entities
{
    public class Role
    {
        [Key]
        public int roleID { get; set; }
        public string Name { get; set; }
    }
}
