﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace BackEnd_Blog.Entities
{
    public class RoleUser
    {
        [Key]
        public int Id { get; set; }
        public string userID { get; set; }
        [ForeignKey(nameof(userID))]
        public virtual User User { get; set; }
        public int roleID { get; set; }
        [ForeignKey(nameof(roleID))]
        public virtual Role Role { get; set; }
    }
}
