﻿using BackEnd_Blog.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace BackEnd_Blog.Entities
{
    public class User
    {
        [Key]
        [Required, EmailAddress]
        public string userID { get; set; }
        [Required]
        public string Password { get; set; }
        [Required]
        public int roleID { get; set; }
        public ICollection<Comment> Comments { get; set; }
    }
}
