﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace BackEnd_Blog.Models
{
    public class AuthenticateModel
    {
        [EmailAddress]
        public string userID { get; set; }

        public string Password { get; set; }
    }
}
