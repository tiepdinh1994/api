﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace BackEnd_Blog.Models
{
    public class BlogViewModel
    {
        public int blogID { get; set; }
        public string? blogTitle { get; set; }
        public string? blogContent { get; set; }
        public string authorID { get; set; }
        public string blogImages { get; set; }
        public string blogComment { get; set; }
        public DateTime? createOn { get; set; }
        public ICollection<Comment> Comments { get; set; }
        public int countCom { get; set; }
    }
}
