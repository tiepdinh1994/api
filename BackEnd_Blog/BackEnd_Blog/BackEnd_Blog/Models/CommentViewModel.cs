﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace BackEnd_Blog.Models
{
    public class CommentViewModel
    {
        public int blogID { get; set; }
        [Required]
        public string content { get; set; }
        public string userID { get; set; }
        public DateTime createOn { get; set; }



    }
}
