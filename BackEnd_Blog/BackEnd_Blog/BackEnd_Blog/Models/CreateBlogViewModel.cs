﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BackEnd_Blog.Models
{
    public class CreateBlogViewModel
    {
        public string Title { get; set; }
        public string Content { get; set; }
        public IFormFile File { get; set; }
        public string AuthorId { get; set; }
        public DateTime createOn { get; set; }
    }
}
