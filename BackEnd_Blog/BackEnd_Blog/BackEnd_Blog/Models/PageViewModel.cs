﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BackEnd_Blog.Models
{
    public class PageViewModel
    {
        public int blogID { get; set; }
        public int CurrentPage { get; set; }
        public int TotalPage { get; set; }
    }
}
