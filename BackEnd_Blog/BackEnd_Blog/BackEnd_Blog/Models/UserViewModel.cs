﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace BackEnd_Blog.Models
{
    public class UserViewModel
    {
        [Required]
        [EmailAddress]
        public string userID { get; set; }
        [Required]
        public string Password { get; set; }
        public int roleID { get; set; }
    }
}
