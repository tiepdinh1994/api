﻿using BackEnd_Blog.Entities;
using BackEnd_Blog.Models;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BackEnd_Blog.Services.IRepository
{
    public interface IAuthenticateService
    {
        string Authenticate(string UserID, string password);
        public bool register([FromBody] UserViewModel users);
    }
}
