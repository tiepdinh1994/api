﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BackEnd_Blog.Models
{
    public interface IBlogRepository
    {
        public List<BlogViewModel> getblog();
        public bool createBlog(CreateBlogViewModel blog, IFormFile files);
        public bool delete(int id);
        Task<Blog> detail(int id);
        public Blog edit(BlogViewModel blog);
        public List<BlogViewModel> GetBlogModel(int page, int pageSize);
        public PageViewModel GetPageModel(int currentPage, int pageSize);
    }
}
