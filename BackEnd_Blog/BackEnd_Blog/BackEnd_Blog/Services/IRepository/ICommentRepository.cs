﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BackEnd_Blog.Models
{
    public interface ICommentRepository
    {
        public bool AddCom(CommentViewModel com);
        public bool deleteComments(int id);
    }
}
