﻿using BackEnd_Blog.Entities;
using BackEnd_Blog.Helper;
using BackEnd_Blog.Models;
using BackEnd_Blog.Services.IRepository;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using static Microsoft.AspNetCore.Razor.Language.TagHelperMetadata;

namespace BackEnd_Blog.Services.Repository
{
    public class AuthenticateService : IAuthenticateService
    {
        private readonly BlogDataContext _context;
        private readonly AppSettings _appSettings;

        public AuthenticateService(BlogDataContext context, IOptions<AppSettings> appSettings)
        {
            _context = context;
            _appSettings = appSettings.Value;
        }

        //Login
        public string Authenticate(string UserID, string password)
        {
            var user = _context.Users.SingleOrDefault(x => x.userID == UserID && x.Password == password);


            if (user == null)
            {
                return null;
            }

            var tokenHandler = new JwtSecurityTokenHandler();
            var key = Encoding.ASCII.GetBytes(_appSettings.Key);
            var tokenDecriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(new Claim[]
                {
                    new Claim("userID",user.userID),
                    new Claim(ClaimTypes.Role, user.roleID.ToString())
                }),
                Expires = DateTime.UtcNow.AddDays(2),
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key),
                SecurityAlgorithms.HmacSha256Signature)
            };
            var token = tokenHandler.CreateToken(tokenDecriptor);
            var test = new JwtSecurityTokenHandler().WriteToken(token);
            return test;

        }


        //register
        public bool register(UserViewModel users)
        {
            var check = _context.Users.Where(p => p.userID == users.userID).FirstOrDefault();
            if (check == null)
            {
                try
                {
                    var newUser = new User
                    {
                        userID = users.userID,
                        Password = Helper.Encryptor.MD5Hash(users.Password),
                        roleID = 2
                    };
                    _context.Users.Add(newUser);
                    _context.SaveChangesAsync();                
                }
                catch(Exception)
                {
                    return false;
                }
            }
            return true;
        }
    }
}
