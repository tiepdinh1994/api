﻿using BackEnd_Blog.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace BackEnd_Blog
{
    public class BlogRepository : IBlogRepository
    {
        private readonly BlogDataContext _context;


        public BlogRepository(BlogDataContext context)
        {
            _context = context;
        }


        //get all listblog
        public List<BlogViewModel> getblog()
        {
            var result = new List<BlogViewModel>();

            var list = _context.Blogs.Include(b => b.Comments).OrderByDescending(b => b.createOn).ToList();

            foreach (var item in list)
            {
                BlogViewModel blog = new BlogViewModel
                {
                    blogID = item.blogID,
                    blogTitle = item.blogTitle,
                    blogContent = item.blogContent,
                    blogImages = item.blogImages,
                    countCom = item.Comments.Count,
                    authorID = item.authorID,
                    createOn = item.createOn
                };
                result.Add(blog);
            }
            return result;
        }


        //get list on page
        public List<BlogViewModel> GetBlogModel(int page, int pageSize)
        {
            if (page <= 0)
            {
                page = 1;
            }

            if (pageSize <= 5)
            {
                pageSize = 5;
            }

            int skip = 0, take = pageSize;

            take = pageSize;
            skip = pageSize * page - pageSize;

            List<BlogViewModel> result = new List<BlogViewModel>();

            List<Blog> queryResult;

            queryResult = _context.Blogs
                   .Include(e => e.Comments)
                .OrderByDescending(blog => blog.createOn)
                .Skip(skip)
                .Take(take).ToList();
            foreach (var i in queryResult)
            {
                result.Add(new BlogViewModel
                {
                    blogID = i.blogID,
                    blogTitle = i.blogTitle,
                    blogContent = i.blogContent,
                    blogImages = i.blogImages,
                    authorID = i.authorID,
                    createOn = i.createOn,
                    countCom = i.Comments.Count
                });
            }
            return result;
        }


        //get page
        public PageViewModel GetPageModel(int currentPage, int pageSize)
        {
            int totalPage = (_context.Blogs.Count() / (pageSize + 1)) + 1;

            return new PageViewModel
            {
                CurrentPage = currentPage,
                TotalPage = totalPage
            };
        }


        //detail blog
        public async Task<Blog> detail(int id)
        {
            return await _context.Blogs
                            .Where(b => b.blogID == id)
                            .Include(b => b.Comments)
                            .FirstOrDefaultAsync();
        }


        //create blog
        public bool createBlog(CreateBlogViewModel blog, IFormFile files)
        {

            string imageURL = "";
            try
            {
                if (files != null && files.Length > 0)
                {
                    imageURL = UploadImage(files);
                    imageURL = imageURL.Substring(imageURL.IndexOf("/UpFileImage"));
                    imageURL = imageURL.Remove(0, "/UpFileImage".Length);
                }


                Blog newBlog = new Blog
                {
                    blogTitle = blog.Title,
                    blogContent = blog.Content,
                    blogImages = imageURL,
                    authorID = blog.AuthorId,
                    createOn = DateTime.Now,
                };
                if (newBlog.blogContent == null || newBlog.blogTitle == null)
                {
                    return false;
                }
                else
                {
                    _context.Blogs.Add(newBlog);
                    _context.SaveChanges();
                    return true;
                }
             
           
            }
            catch
            {
                return false;
            }


        }


        //delete
        public bool delete(int id)
        {
            try
            {
                var comments = _context.Comments.Where(c => c.blogID == id).ToList();
                _context.Comments.RemoveRange(comments);
                var blog = _context.Blogs.Find(id);
                if (blog != null)
                {
                    _context.Blogs.Remove(blog);
                }
                _context.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }

        }


        //edit
        public Blog edit(BlogViewModel blog)
        {
            var check = _context.Blogs.Where(p => p.blogID == blog.blogID).FirstOrDefault();
            if (check != null)
            {
                if (blog.blogTitle == null)
                {
                    blog.blogTitle = check.blogTitle;
                }

                check.blogTitle = blog.blogTitle;

                if (blog.blogContent == null)
                {
                    blog.blogContent = check.blogContent;
                }

                check.blogContent = blog.blogContent;

                if (blog.blogImages == null)
                {
                    blog.blogImages = check.blogImages;
                }

                check.blogImages = blog.blogImages;
            }
            _context.SaveChanges();

            return check;

        }


        //Upload Images
        private string UploadImage(IFormFile file)
        {
            string Url = Directory.GetCurrentDirectory();
            var guiId = Guid.NewGuid().ToString();
            var filename = guiId + file.FileName;
            string filePath = "/FileImage/assets/UpFileImage/" + filename;
            filePath = Url + filePath.Remove(filePath.LastIndexOf("."));

            string fileThumbPath = "/FileImage/assets/Images/" + filename;
            fileThumbPath = Url + fileThumbPath.Remove(fileThumbPath.LastIndexOf("."));

            using (var stream = new FileStream(filePath, FileMode.Create))
            {
                file.CopyTo(stream);
            }
            using (var stream = new FileStream(fileThumbPath, FileMode.Create))
            {
                file.CopyTo(stream);
            }
            Resize(filePath, filePath + ".jpg", 600);
            Resize(fileThumbPath, fileThumbPath + ".jpg", 120);

            System.IO.File.Delete(filePath);
            System.IO.File.Delete(fileThumbPath);

            return filePath.Replace("FileImage/Assets", "") + ".jpg";
        }


        //Resize image
        private void Resize(string input_file, string output_file, int new_Width)
        {
            const long quality = 100L;
            Bitmap source_Bitmap = new Bitmap(input_file);
            double dblWidth_origial = source_Bitmap.Width;
            double dblHeigth_origial = source_Bitmap.Height;
            double relation_heigth_width = dblHeigth_origial / dblWidth_origial;
            int new_Height = (int)(new_Width * relation_heigth_width);
            var new_Pic = new Bitmap(new_Width, new_Height);
            using (var setup_graphic = Graphics.FromImage(new_Pic))
            {
                //setup
                setup_graphic.CompositingQuality = CompositingQuality.HighSpeed;
                setup_graphic.InterpolationMode = InterpolationMode.HighQualityBicubic;
                setup_graphic.CompositingMode = CompositingMode.SourceCopy;
                //draw into placeholder
                setup_graphic.DrawImage(source_Bitmap, 0, 0, new_Width, new_Height);

                //Output as .Jpg 
                using (var output = System.IO.File.Open(output_file, FileMode.Create))
                {
                    //setup jpg
                    var qualityParamId = Encoder.Quality;
                    var encoderParameters = new EncoderParameters(1);
                    encoderParameters.Param[0] = new EncoderParameter(qualityParamId, quality);

                    //save as Jpg
                    var codec = ImageCodecInfo.GetImageDecoders().FirstOrDefault(c => c.FormatID == ImageFormat.Jpeg.Guid);
                    new_Pic.Save(output, codec, encoderParameters);
                    output.Close();
                }
                setup_graphic.Dispose();
            }
            source_Bitmap.Dispose();
        }

    }
}