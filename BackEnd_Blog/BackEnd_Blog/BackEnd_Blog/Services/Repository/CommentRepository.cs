﻿using BackEnd_Blog.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BackEnd_Blog
{
    public class CommentRepository : ICommentRepository
    {
        private readonly BlogDataContext _context;
        public CommentRepository(BlogDataContext context)
        {
            _context = context;
        }

        
        //add comment
        public bool AddCom(CommentViewModel com)
        {
            var b = _context.Blogs.Where(p=>p.blogID == com.blogID).FirstOrDefault();
            if (b != null)
            {
                Comment comment = new Comment
                {
                    blogID = com.blogID,
                    content = com.content,
                    userID = com.userID,
                    creatOn = DateTime.Now
                };
                _context.Comments.Add(comment);
                _context.SaveChanges();
                return true;
            }
            else
                return false;

        }

        
        //delete comment
        public bool deleteComments(int id)
        {
            try
            {
                IEnumerable<Comment> listCom = _context.Comments.Where(p => p.commentID == id).ToList();
                foreach (var c in listCom)
                {
                    _context.Remove(c);
                    _context.SaveChanges();                  
                }
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

    }
  

}
