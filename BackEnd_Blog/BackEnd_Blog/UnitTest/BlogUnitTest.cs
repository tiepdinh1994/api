﻿using BackEnd_Blog;
using BackEnd_Blog.Controllers;
using BackEnd_Blog.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Moq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Xunit;

namespace UnitTest
{
    public class BlogUnitTest
    {
        [Fact(DisplayName = "List blog")] //pass
        public void GetBlog()
        {
            //Arrange
            var options = new DbContextOptionsBuilder<BlogDataContext>()
            .UseInMemoryDatabase(databaseName: "GetBlog").Options;
            var context = new BlogDataContext(options);
            DbContextExtensions.Seed(context);
            var repo = new BlogRepository(context);
            //Actual
            var results = repo.getblog();
            var count = results.Count;
            //Assert
            Assert.Equal(3, count);

        }


        [Fact(DisplayName = "GetBlogModel")] // pass
        public void GetBlogModel()
        {
            //Arrange
            var options = new DbContextOptionsBuilder<BlogDataContext>()
            .UseInMemoryDatabase(databaseName: "GetBlogModel").Options;
            var context = new BlogDataContext(options);
            DbContextExtensions.Seed(context);
            var repo = new BlogRepository(context);
            //Actual
            var results = repo.GetBlogModel(1, 5);
            var count = results.Count;
            //Assert
            Assert.Equal(3, count);

        }


        [Fact(DisplayName = "GetPageModel")] //pass
        public void GetPageModel()
        {
            //Arrange
            var options = new DbContextOptionsBuilder<BlogDataContext>()
            .UseInMemoryDatabase(databaseName: "GetPageModel").Options;
            var context = new BlogDataContext(options);
            DbContextExtensions.Seed(context);
            var repo = new BlogRepository(context);
            //Actual
            var results = repo.GetPageModel(1, 5);
            //Assert
            Assert.Equal(1, results.TotalPage);

        }


        [Fact(DisplayName = "Details")] //pass
        public async Task Details()
        {
            //Arrange
            var options = new DbContextOptionsBuilder<BlogDataContext>()
            .UseInMemoryDatabase(databaseName: "Details").Options;
            var context = new BlogDataContext(options);
            DbContextExtensions.Seed(context);
            var repo = new BlogRepository(context);
            //Actual
            var result = await repo.detail(2);
            //Assert
            Assert.Equal(2,result.blogID);

        }
      

        [Fact(DisplayName = "Delete")] //Delete found blogID
        public void Delete()
        {
            //Arrange
            var options = new DbContextOptionsBuilder<BlogDataContext>()
            .UseInMemoryDatabase(databaseName: "Delete").Options;
            var context = new BlogDataContext(options);
            DbContextExtensions.Seed(context);
            var repo = new BlogRepository(context);
            //Actual
            var result = repo.delete(2);
            //Assert
            Assert.True(result);

        }


        [Fact(DisplayName = "Delete_NotFoundID")] //Delete not found blogID
        public void Delete_NotFoundID()
        {
            //Arrange
            var options = new DbContextOptionsBuilder<BlogDataContext>()
            .UseInMemoryDatabase(databaseName: "Delete_NotFoundID").Options;
            var context = new BlogDataContext(options);
            DbContextExtensions.Seed(context);
            var repo = new BlogRepository(context);
            //Actual
            var result = repo.delete(5);
            //Assert
            Assert.False(result);

        }


        [Fact(DisplayName = "createBlog")] //pass
        public void createBlog()
        {
            //Arrange
            var options = new DbContextOptionsBuilder<BlogDataContext>()
            .UseInMemoryDatabase(databaseName: "createBlog").Options;
            var context = new BlogDataContext(options);
            DbContextExtensions.Seed(context);
            var repo = new BlogRepository(context);
            var createBlogViewModel = new CreateBlogViewModel
            {
                AuthorId = "admin@gmail.com",
                Title = "Title 5",
                Content = "Content 5",
                createOn = DateTime.Now,
            };
            var result = repo.createBlog(createBlogViewModel, null);
            Assert.True(result);

        }


        [Fact(DisplayName = "createBlog_NullTitle")] //No title
        public void createBlog_NullTitle()
        {
            //Arrange
            var options = new DbContextOptionsBuilder<BlogDataContext>()
            .UseInMemoryDatabase(databaseName: "createBlog_NullTitle").Options;
            var context = new BlogDataContext(options);
            DbContextExtensions.Seed(context);
            var repo = new BlogRepository(context);
            var createBlogViewModel = new CreateBlogViewModel
            {
                AuthorId = "admin@gmail.com",
                Title = null,
                Content = "Content 5",
                createOn = DateTime.Now,
            };
            var result = repo.createBlog(createBlogViewModel, null);
            Assert.False(result);

        }


        [Fact(DisplayName = "createBlog_NullContent")] //No content
        public void createBlog_NullContent()
        {
            //Arrange
            var options = new DbContextOptionsBuilder<BlogDataContext>()
            .UseInMemoryDatabase(databaseName: "createBlog_NullContent").Options;
            var context = new BlogDataContext(options);
            DbContextExtensions.Seed(context);
            var repo = new BlogRepository(context);
            var createBlogViewModel = new CreateBlogViewModel
            {
                AuthorId = "admin@gmail.com",
                Title = "Title 5",
                Content = null,
                createOn = DateTime.Now,
            };
            var result = repo.createBlog(createBlogViewModel, null);
            Assert.False(result);

        }




    }
}
