﻿using BackEnd_Blog;
using BackEnd_Blog.Models;
using Microsoft.EntityFrameworkCore;
using Moq;
using System;
using System.Collections.Generic;
using System.Text;
using Xunit;

namespace UnitTest
{
    public class CommentUnitTest
    {
        //private void SetupMocks()


        [Fact(DisplayName = "AddCom")] // Found blogID
        public void AddComSucces()
        {
            //Arrange
            var options = new DbContextOptionsBuilder<BlogDataContext>()
            .UseInMemoryDatabase(databaseName: "AddCom").Options;
            var context = new BlogDataContext(options);
            DbContextExtensions.Seed(context);
            var repo = new CommentRepository(context);
            var result = repo.AddCom(new CommentViewModel()
            {
                content = "adasda",
                createOn = DateTime.Now,
                userID = "admin@gmail.com",
                blogID = 1
            });

            //Assert
            Assert.True(result);
        }

        [Fact(DisplayName = "AddCom_BlogIDNotFound")] //Not Found blogID
        public void AddCom_BlogIDNotFound ()
        {
            //Arrange
            var options = new DbContextOptionsBuilder<BlogDataContext>()
            .UseInMemoryDatabase(databaseName: "AddCom").Options;
            var context = new BlogDataContext(options);
            DbContextExtensions.Seed(context);
            var repo = new CommentRepository(context);
            var result = repo.AddCom(new CommentViewModel()
            {
                content = "abc",
                createOn = DateTime.Now,
                userID = "admin@gmail.com",
                blogID = 10 //blogID not found
            });

            //Assert
            Assert.False(result);
        }
    }
}
