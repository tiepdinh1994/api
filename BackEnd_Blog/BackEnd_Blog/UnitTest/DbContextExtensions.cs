﻿using BackEnd_Blog.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace UnitTest
{
    public static class DbContextExtensions
    {
        public static void Seed(this BlogDataContext DBContext)
        {
           
            //Blog
            DBContext.Blogs.AddRange(
            new Blog
            {
                blogID = 1,
                blogTitle = "Title 1",
                blogContent = "Content 1",
                blogImages = "Images 1",
                createOn = Convert.ToDateTime("5/31/2016 11:11:00 AM"),
                authorID = "admin@gmail.com"

            },
            new Blog
            {
                blogID = 2,
                blogTitle = "Title 2",
                blogContent = "Content 2",
                blogImages = "Images 2",
                createOn = Convert.ToDateTime("5/21/2017 12:51:00 PM"),
                authorID = "admin@gmail.com"

            },
            new Blog
            {
                blogID = 3,
                blogTitle = "Title 3",
                blogContent = "Content 3",
                blogImages = "Images 3",
                createOn = Convert.ToDateTime("4/14/2016 10:17:00 PM"),
                authorID = "admin@gmail.com"

            });
           // DBContext.Blogs.AddRange();
            DBContext.SaveChanges();


            //User
            DBContext.Users.AddRange(new BackEnd_Blog.Entities.User
            {
                userID="admin@gmail.com",
                Password="123456",
                roleID =1
            },
            new BackEnd_Blog.Entities.User
            {
                userID = "admin1@gmail.com",
                Password = "123456",
                roleID = 1
            },
            new BackEnd_Blog.Entities.User
            {
                userID = "user1@gmail.com",
                Password = "123456",
                roleID = 2
            },
            new BackEnd_Blog.Entities.User
            {
                userID = "user2@gmail.com",
                Password = "123456",
                roleID = 2
            });
           // DBContext.Users.AddRange();
            DBContext.SaveChanges();


            //Comment
            DBContext.Comments.AddRange(new Comment
            {
                blogID = 1,
                content = "Nice",
                creatOn = Convert.ToDateTime("5/31/2016 11:21:00 PM"),
                userID = "user@gmail.com"
            },
            new Comment
            {
                blogID = 1,
                content = "Nice",
                creatOn = Convert.ToDateTime("5/31/2016 11:25:00 PM"),
                userID = "admin@gmail.com"
            },
            new Comment
            {
                blogID = 2,
                content = "Bad",
                creatOn = Convert.ToDateTime("5/31/2016 11:25:00 PM"),
                userID = "user2@gmail.com"
            },
            new Comment
            {
                blogID = 3,
                content = "Not really good",
                creatOn = Convert.ToDateTime("5/31/2016 11:25:00 PM"),
                userID = "admin@gmail.com"
            });
          //  DBContext.Comments.AddRange();
            DBContext.SaveChanges();
        }
    }
}
