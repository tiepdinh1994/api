﻿using BackEnd_Blog.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace UnitTest
{
    public static class DbContextMocker
    {
        public static BlogDataContext DataContext(string dbName)
        {
            var options = new DbContextOptionsBuilder<BlogDataContext>()
                .UseInMemoryDatabase(databaseName: dbName)
                .Options;
            var dbContext = new BlogDataContext(options);
            dbContext.Seed();
            return dbContext;

        }
    }
}
