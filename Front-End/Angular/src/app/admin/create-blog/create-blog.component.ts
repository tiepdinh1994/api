import { Component, OnInit, ViewChild } from '@angular/core';
import { DeleteBlogService } from '../../services/blogs/delete-blog.service';
import { blogpost } from '../../models/blogpost';
import { Router, ActivatedRoute } from '@angular/router';
import { CreateBlogService } from 'src/app/services/blogs/create-blog.service';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';

@Component({
  selector: 'app-create-blog',
  templateUrl: './create-blog.component.html',
  styleUrls: ['./create-blog.component.css']
})
export class CreateBlogComponent implements OnInit {
  createForm: FormGroup;


  constructor(private fb: FormBuilder, private route: ActivatedRoute, private createBlogService: CreateBlogService,
    private router: Router, ) { }

  submitted = false;
  selectedFile: File

  ngOnInit(): void {

  }
  selectFile($event) {
    var file = $event.target.files[0];
    console.log($event.target.files.length)
    if ($event.target.files.length > 0) {
      this.selectedFile = file;
    }
    else {
      this.selectedFile = null
    }
  }
  createPost(title, con) {
    if (localStorage.getItem("userID") != undefined) {
      this.createBlogService.createBlog(title, con, this.selectedFile).subscribe(resp => {

        if ((resp === false)) {
          alert("Please input value title or content!");
        }
        if (resp === true) {
          alert("Create successed ! Redirected homepage in 2 second");
          setTimeout(() => {
            this.router.navigate(['/'])
          }, 2000);
        }
      });
    }
  }
}
