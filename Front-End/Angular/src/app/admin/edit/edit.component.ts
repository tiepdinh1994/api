import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { BlogDetailService } from '../../services/blogs/blog-detail.service';
import { EditBlogService } from 'src/app/services/blogs/edit-blog.service';

@Component({
  selector: 'app-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.css']
})
export class EditComponent implements OnInit {
  
  submitted = false;
  selectedFile: File;
  blogID: number;
  constructor(private route: ActivatedRoute,
    private editBlogService: EditBlogService,
     private blogDetailService: BlogDetailService) { }

  ngOnInit(): void {
  }
  selectFile($event) {
    var file = $event.target.files[0];
    console.log($event.target.files.length)
    if ($event.target.files.length > 0) {
      this.selectedFile = file;
    }
    else {
      this.selectedFile = null
    }
  }
  edit(con, title) {
    if (localStorage.getItem("userID") != undefined) {
      this.editBlogService.editBlog(con, title, this.selectFile).subscribe(resp => {
        alert(123);
      });
    }
  }
}
