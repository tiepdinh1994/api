import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { BlogsComponent } from './blogs/blogs.component';
import { BlogDetailComponent } from './blog-detail/blog-detail.component';
import { CreateBlogComponent } from './admin/create-blog/create-blog.component';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { EditComponent } from './admin/edit/edit.component';


const routes: Routes = [
  { path: '', component: BlogsComponent, pathMatch: 'full' },
  { path: 'detailsBlog', component: BlogDetailComponent },
  { path: 'login', component: LoginComponent },
  { path: 'createBlog', component: CreateBlogComponent },
  { path: 'register', component: RegisterComponent },
  { path: 'edit', component: EditComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
