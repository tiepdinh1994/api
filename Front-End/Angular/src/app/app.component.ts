import { Component } from '@angular/core';
import { LoginService } from './services/users/login.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'Angular';
  constructor(private _loginService : LoginService){};
}
