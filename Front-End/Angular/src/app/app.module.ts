import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BlogsComponent } from './blogs/blogs.component';
import { BlogDetailComponent } from './blog-detail/blog-detail.component';
import { HeaderComponent } from './layout/header/header.component';
import { FooterComponent } from './layout/footer/footer.component';
import { HttpClientModule } from '@angular/common/http';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { CommentComponent } from './Comment-Folder/comment/comment.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { LoginActiveComponent } from './login_template/login-active/login-active.component';
import { DeleteCommentComponent } from './comment-folder/delete-comment/delete-comment.component';
import { BlogPostService } from './services/blogs/blog-post.service';
import { DeleteBlogService } from './services/blogs/delete-blog.service';
import { LoginService } from './services/users/login.service';
import { BlogDetailService } from './services/blogs/blog-detail.service';
import { PostCommentService } from './services/comments/post-comment.service';
import { CreateBlogComponent } from './admin/create-blog/create-blog.component';
import { CreateBlogService } from './services/blogs/create-blog.service';
import { RegisterService } from './services/users/register.service';
import { PageComponent } from './page/page.component';
import { EditComponent } from './admin/edit/edit.component';
import { EditBlogService } from './services/blogs/edit-blog.service';
@NgModule({
  declarations: [
    AppComponent,
    BlogsComponent,
    BlogDetailComponent,
    HeaderComponent,
    FooterComponent,
    LoginComponent,
    RegisterComponent,
    CommentComponent,
    LoginActiveComponent,
    DeleteCommentComponent,
    CreateBlogComponent,
    PageComponent,
    EditComponent
  

  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule, 
    FormsModule,                              
    ReactiveFormsModule
  ],
  providers: [BlogPostService, PostCommentService, BlogDetailService, CreateBlogService,
  RegisterService, DeleteBlogService, LoginService, EditBlogService],
  bootstrap: [AppComponent]
})
export class AppModule { }
