import { Component, OnInit, Input } from '@angular/core';
import { BlogDetailService } from '../services/blogs/blog-detail.service';
import { Router, ActivatedRoute } from '@angular/router'
import { compost } from '../models/compost';
import { PostCommentService } from '../services/comments/post-comment.service';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { DeleteBlogService } from '../services/blogs/delete-blog.service';
@Component({
  selector: 'app-blog-detail',
  templateUrl: './blog-detail.component.html',
  styleUrls: ['./blog-detail.component.css']
})
export class BlogDetailComponent implements OnInit {
  blogPosts: any;
  blogID: number;
  comPosts: any;
  userID: any;
  clickComment: boolean = false;
  isLogin: boolean = false;
  commentForm: FormGroup;
  content: string;

  constructor(private route: ActivatedRoute, public blogDetailService: BlogDetailService
    , private fb: FormBuilder, private router: Router,
    private deleteBlogService: DeleteBlogService,
    private postCommentService: PostCommentService) { }

  ngOnInit(): void {
    var queryParams: any = this.route.queryParams
    this.blogID = queryParams._value.id
    this.loadBlogsComment();
    this.loadBlogs();
    this.check();
    this.commentForm = new FormGroup({
      content: new FormControl('', Validators.required)
    });

  }
  //load
  loadBlogs() {
    this.blogDetailService.getdetails(this.blogID).subscribe(resp => {
      this.blogPosts = resp
    });
  }

  //load detail
  loadBlogsComment() {
    this.blogDetailService.getdetails(this.blogID).subscribe(resp => {
      console.log(resp)
      this.comPosts = resp
    }, error => {
      alert(error.message)
      console.log(error)
    })
  }

  refresh() {
    location.reload();
  }

  //create com
  createCom() {
    var com = new compost();
    com.blogID = this.blogID;
    com.content = this.commentForm.value['content']

    //com.content = content;
    this.postCommentService.createCom(com).subscribe(response => {
      {
        this.clickComment = false;
        this.ngOnInit();
      }
    })
  }

  //check login
  check() {
    if (localStorage.getItem("userID") != undefined) {
      this.isLogin = true;
    }
    else {
      this.isLogin = false;
    }
  }
}
