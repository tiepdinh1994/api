import { Component, OnInit } from '@angular/core';
import { BlogPostService } from '../services/blogs/blog-post.service';
import { ActivatedRoute, Router } from '@angular/router';
@Component({
  selector: 'app-blogs',
  templateUrl: './blogs.component.html',
  styleUrls: ['./blogs.component.css']
})
export class BlogsComponent implements OnInit {
  blogPosts: any
  pagingModel: any
  page = 0
  constructor(public blogPostService: BlogPostService,
    private router: Router, private activatedRoute: ActivatedRoute) {
  }
  ngOnInit(): void {

    this.activatedRoute.queryParams.subscribe(p => {
      this.loadBlog()
    })
  }

  loadBlog() {
    var queryParams: any = this.activatedRoute.queryParams

    if (queryParams._value.page) {
      this.page = queryParams._value.page
    }

    this.blogPostService.listBlog(this.page).subscribe(response => {
      // console.log(resp)
      this.blogPosts = response
    })

    this.blogPostService.getPage(this.page).subscribe(response => {
      this.pagingModel = response
      console.log(this.pagingModel)
    })
  }
}


