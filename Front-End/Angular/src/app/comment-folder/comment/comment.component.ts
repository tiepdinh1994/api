import { Component, OnInit } from '@angular/core';
import { PostCommentService } from 'src/app/services/comments/post-comment.service';
import { Router } from '@angular/router';
import { compost } from 'src/app/models/compost';

@Component({
  selector: 'app-comment',
  templateUrl: './comment.component.html',
  styleUrls: ['./comment.component.css']
})
export class CommentComponent implements OnInit {

  constructor(private postCommentService: PostCommentService, private router: Router) { }

  ngOnInit(): void {
  }
  
}
