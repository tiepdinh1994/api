import { Component, OnInit } from '@angular/core';
import { LoginService } from 'src/app/services/users/login.service';
import { Router } from '@angular/router';
import * as decode from 'jwt-decode';
@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})

export class HeaderComponent implements OnInit {
  userID: any;
  roleID: any;
  isLogin: boolean = false;
  isAdmin: boolean = false;
  constructor(public loginService: LoginService, private router: Router) { }

  ngOnInit(): void {
    this.check();
    this.checkrole();
  }

  checkrole() {
    if (localStorage.getItem("token") != undefined) {
      var decoded = decode(localStorage.getItem("token"));
      this.roleID = decoded.role;
      if (this.roleID == 1) {
        this.isAdmin = true;

      } else {
        this.isAdmin = false;
      }
    }
  }

  check() {
    if (localStorage.getItem("userID") != undefined) {
      this.userID = localStorage.getItem("userID");
      // alert(this.userID);
      this.router.navigate(['/']);

      this.isLogin = true;


    }
    else {
      this.isLogin = false;

    }
  }

  logout() {
    localStorage.removeItem('token');
    localStorage.removeItem('userID');
    this.isLogin = false;
    window.location.reload();
    this.router.navigate(['/']);
  }

}
