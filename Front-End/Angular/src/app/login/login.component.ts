import { Component, OnInit } from '@angular/core';
import { LoginService } from '../services/users/login.service';
import { NgForm, FormBuilder, Validators, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  loginForm: FormGroup;
  loading = false;
  submitted = false;
  constructor(private fb: FormBuilder, public loginService: LoginService, private router: Router) { }

  ngOnInit(): void {
    this.loginForm = this.fb.group({
      userID: ['', Validators.email],
      Password: ['', [Validators.required, Validators.minLength(6)]]
    })
  }
  get f() { return this.loginForm.controls; }

  onSubmit() {
    this.submitted = true;
    if (this.loginForm.invalid) {
      return;
    }
    this.loginService.login(this.loginForm.value).subscribe(
      (response: any) => {
        if (response.status === 404) {
          this.loading = false;
          alert('Email or Password is incorrect');
        }
        else {
          localStorage.setItem('token', response.token);
          localStorage.setItem('userID', response.userID);//get info
          window.location.reload();
          this.router.navigateByUrl[('/')];
        }
      },
      error => {
        alert('Email or Password is incorrect');
      });
  }


  isLogin() {

    if (localStorage.getItem("userID") != undefined) {
      // window.location.reload();
      this.router.navigateByUrl('/');
    }

  }
}