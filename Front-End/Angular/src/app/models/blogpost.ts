export class blogpost{
    blogID: number;
    blogTitle: string;
    blogContent: string;
    createOn: string;
    authorID: string;
    blogImages: string;
    commentID: number;
    countCom: number;
}