import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-page',
  templateUrl: './page.component.html',
  styleUrls: ['./page.component.css']
})
export class PageComponent implements OnInit {
  @Input() totalPage = 1
  @Input() currentPage = 1
  @Input() loadPage = false
  PageButton = []
  constructor() { }

  ngOnInit(): void {
    this.EditPage()
  }
  ngOnChanges(changes: PageComponent) {
    this.EditPage()
  }
  EditPage(){
    this.PageButton= []

    if(this.currentPage == 0)
      this.currentPage = 1
    
      for (var i = 1; i <= this.totalPage; i++)
      {
        this.PageButton.push(i)
      }
    }
}
