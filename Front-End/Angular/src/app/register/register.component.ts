import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { RegisterService } from '../services/users/register.service';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  registerForm: FormGroup;
  loading = false;
  submitted = false;

  constructor(private fb: FormBuilder, private router: Router, private registerService: RegisterService) { }

  ngOnInit(): void {
    this.registerForm = this.fb.group({
      userID: ['', Validators.email],
      Password: ['', [Validators.required, Validators.minLength(6)]]
    });
  }
  get f() { return this.registerForm.controls; }
  onSubmit() {
    this.submitted = true;
    if (this.registerForm.invalid) {
      return;
    }

    this.loading = true;
    this.registerService.register(this.registerForm.value).subscribe(resp => {
      if (resp === true) {
        alert("Sign up successed!");
        setTimeout(() => {
          this.router.navigate(['login'])
        }, 3000);
      }
      else {
        this.loading = false;
        alert("Failed. Your email has been be registed. Please try another email");
      }

    }, error => {
      this.loading = false;
      alert("Sever Error!");
    });
  }
}