import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { blogpost } from '../../models/blogpost';

@Injectable({
  providedIn: 'root'
})
export class BlogDetailService {
  myAppUrl: string;
  myApiUrl: string;

  constructor(private http: HttpClient) {
    this.myAppUrl = 'http://localhost:59005/';
    this.myApiUrl = 'home/detail/';
  }
  getdetails(blogID: number): Observable<blogpost> {
    // alert(blogID)
    return this.http.get<blogpost>(this.myAppUrl + this.myApiUrl + blogID)
  };

  getcomments(blogID: number): Observable<blogpost> {
    // alert(blogID)
    return this.http.get<blogpost>(this.myAppUrl + this.myApiUrl + blogID)
  };
}

