import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { blogpost } from '../../models/blogpost';
@Injectable({
  providedIn: 'root'
})
export class BlogPostService {
  myAppUrl: string;
  myApiUrl: string;
  myDetailUrl: string;
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json; charset=utf-8'
    })
  }
  constructor(private http: HttpClient) {

    this.myAppUrl = 'http://localhost:59005/';
    this.myApiUrl = 'home/getblog';
  }
  //get all blog
  getBlogs(): Observable<any> {
    return this.http.get(this.myAppUrl + this.myApiUrl)
  };
  //get details
  getBlogPost(blogID: number): Observable<blogpost> {
    return this.http.get<blogpost>(this.myAppUrl + this.myApiUrl + blogID)
  };
  //get blog by page
  listBlog(page) {
    return this.http.get(this.myAppUrl + "home/getblogs?page=" + page);
  }
  //get page 
  getPage(currentPage) {
    return this.http
      .get(this.myAppUrl + "home/getPage", { params: { page: currentPage } });
  }
}
