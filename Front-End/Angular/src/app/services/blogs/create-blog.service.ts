import { Injectable } from '@angular/core';
import { blogpost } from 'src/app/models/blogpost';
import { Observable } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class CreateBlogService {
  myAppUrl: string;
  myApiUrl: string;
  token = 'Bearer ' + localStorage.getItem('token');
  httpOptions = {
    headers: new HttpHeaders({
      'Authorization': this.token
    })
  }

  constructor(private http: HttpClient) {
    this.myAppUrl = 'http://localhost:59005/Admin/Create';
  }


  createBlog(title, con, file) {

    var from = new FormData();
    from.append("Title", title)
    from.append("Content", con)
    from.append("File", file)

    return this.http.post(this.myAppUrl, from, this.httpOptions);
  }
}
