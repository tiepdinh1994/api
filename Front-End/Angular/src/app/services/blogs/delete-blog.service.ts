import { Injectable, ViewChild } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { blogpost } from '../../models/blogpost';


@Injectable({
  providedIn: 'root'
})
export class DeleteBlogService {
  myAppUrl: string;

  constructor(private http: HttpClient) {
    this.myAppUrl = 'http://localhost:59005/home/delete/';
  }
  deleteBlog(blogID: number) {
    return this.http.delete<blogpost>(this.myAppUrl + blogID);
  }
}

