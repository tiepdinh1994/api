import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { blogpost } from 'src/app/models/blogpost';

@Injectable({
  providedIn: 'root'
})
export class EditBlogService {
  myAppUrl: string;
  blogID: number;
  token = 'Bearer ' + localStorage.getItem('token');
  httpOptions = {
    headers: new HttpHeaders({
      'Authorization': this.token
    })
  }

  constructor(private http: HttpClient) {
    this. myAppUrl ="http://localhost:59005/Admin/edit/";
  }
  editBlog(title, con, file) {

    var from = new FormData();
    from.append("Title", title)
    from.append("Content", con)
    from.append("File", file)

    return this.http.post(this.myAppUrl + this.blogID, from, this.httpOptions);
  }
}
