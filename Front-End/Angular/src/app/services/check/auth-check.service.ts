import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { HttpRequest, HttpHandler, HttpEvent } from '@angular/common/http';
import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class AuthCheckService {

  constructor(private router: Router) {

   }
   check(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    if(localStorage.getItem('token')!=null){
      const cloneReq = request.clone({
        headers: request.headers.set('Authorization','Bearer '+localStorage.getItem('token'))
      });
      return next.handle(cloneReq).pipe(
        tap(
          success => {

          },
          error => {
            if(error.status == 401)
            {
              localStorage.removeItem('token')
              this.router.navigateByUrl('/login')
            }
          }
        )
      )
    }else{
      return next.handle(request.clone());
    }

  }
}
