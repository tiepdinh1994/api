import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { compost } from 'src/app/models/compost';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class PostCommentService {
  myAppUrl: string;
  myApiUrl: string;
  token = 'Bearer ' + localStorage.getItem('token');
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json; charset=utf-8',
      'Authorization': this.token
    })
  }

  constructor(private http: HttpClient) {
    this.myAppUrl = "http://localhost:59005/";
    this.myApiUrl = "Comment/AddCom";
  }

  createCom(newCom: compost): Observable<compost> {
    return this.http.post<compost>(this.myAppUrl + this.myApiUrl, newCom, this.httpOptions);
  }
}
