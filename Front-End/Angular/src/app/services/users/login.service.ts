import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Router } from '@angular/router';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { users } from 'src/app/models/user';

@Injectable({
  providedIn: 'root'
})
export class LoginService {
  myAppUrl: string;
  myApiUrl: string;


  constructor(private fb: FormBuilder, private http: HttpClient, private _router: Router) {
    this.myAppUrl = "http://localhost:59005";
    this.myApiUrl = "/UserProfile/login";
  }
  login(user: users) {
    return this.http.post(this.myAppUrl + this.myApiUrl, user);
  };

  loggedin() {
    return !!localStorage.getItem('token');
  }

  getUserProfile() {
    return this.http.get(this.myAppUrl + "/UserProfile/infoUser");
  }
}
