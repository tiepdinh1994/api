import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { users } from '../../models/user';
import { Observable } from 'rxjs';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
@Injectable({
  providedIn: 'root'
})
export class RegisterService {
  myAppUrl: string;
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json; charset=utf-8'
    })
  }
  constructor(private http: HttpClient, private fb: FormBuilder) {
    this.myAppUrl = "http://localhost:59005/UserProfile/register";
  }

  register(user: users) {
    return this.http.post(this.myAppUrl, user);
  }


}
